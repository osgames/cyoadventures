-------------------------------------------------------------------------------
--                                                                           --
--  Filename: play_adventure_game.adb                                        --
--                                                                           --
--  Date:     7/24/2012                                                      --
--                                                                           --
--  Purpose:  Specification of "play a game" procedures.                     --
--            (See cyo_adventures_main.adb for full program description.     --
--                                                                           --
--  Legal licensing note:                                                    --
--                                                                           --
--    Copyright (c) 2012 Matt Davison                                        --
--                                                                           --
--  This program is free software; you can redistribute it and/or modify it  --
--  under the terms of the GNU General Public License as published by the    --
--  Free Software Foundation; either version 2 of the License, or (at your   --
--  option) any later version. This program is distributed in the hope that  --
--  it will be useful, but WITHOUT ANY WARRANTY; without even the implied    --
--  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See    --
--  the GNU General Public License for more details.  You should have        --
--  received a copy of the GNU General Public License along with this        --
--  program; if not, write to the Free Software Foundation, Inc., 59 Temple  --
--  Place - Suite 330, Boston, MA 02111-1307, USA.                           --
--                                                                           --
-------------------------------------------------------------------------------

with Ada.Strings.Unbounded;

package Play_Game is

  package StrUnbounded renames Ada.Strings.Unbounded;

  -- Load list of ".cyo" files in the "games" directory.  Get the user's
  -- selection, and run the game.
  procedure Play_Saved_Game;

  -- List games in the "games" directory, and load the user's selection.
  procedure Get_Game_Selection (Total_Game : out
                                StrUnbounded.Unbounded_String);

  -- Run the game in the parameter Total_Game.
  procedure Run_Game(Total_Game : in StrUnbounded.Unbounded_String);

end Play_Game;
