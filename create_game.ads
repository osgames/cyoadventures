-------------------------------------------------------------------------------
--                                                                           --
--  Filename: create_adventure_game.ads                                      --
--                                                                           --
--  Date:     7/24/2012                                                      --
--                                                                           --
--  Purpose:  Specification of "create a new game" procedures.               --
--            (See cyo_adventures_main.adb for full program description.     --
--                                                                           --
--  Legal licensing note:                                                    --
--                                                                           --
--    Copyright (c) 2012 Matt Davison                                        --
--                                                                           --
--  This program is free software; you can redistribute it and/or modify it  --
--  under the terms of the GNU General Public License as published by the    --
--  Free Software Foundation; either version 2 of the License, or (at your   --
--  option) any later version. This program is distributed in the hope that  --
--  it will be useful, but WITHOUT ANY WARRANTY; without even the implied    --
--  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See    --
--  the GNU General Public License for more details.  You should have        --
--  received a copy of the GNU General Public License along with this        --
--  program; if not, write to the Free Software Foundation, Inc., 59 Temple  --
--  Place - Suite 330, Boston, MA 02111-1307, USA.                           --
--                                                                           --
-------------------------------------------------------------------------------

with Ada.Strings.Unbounded;

package Create_Game is

  package StrUnbounded renames Ada.Strings.Unbounded;

  -- Create new game.
  procedure Create_New_Game;

  -- Prompt user for game's introduction, and save in Total_Game.
  procedure Get_Game_Introduction (Total_Game : out
                                   StrUnbounded.Unbounded_String);

  -- Get rest of the game after the introduction.  Save in Total_Game.
  procedure Get_Game_Branches (Total_Game : in out
                               StrUnbounded.Unbounded_String);

  -- Save game in the "games" directory.
  procedure Save_Completed_Game (Game_Name : in out String;
                                 Game_Name_Length : in out Integer;
                                 Total_Game : in
                                 StrUnbounded.Unbounded_String);

  -- Return true if file Name exists.  Return false if it does not exist.
  function File_Exists (Name : String) return Boolean;

  -- Search backwards from the end of the total game to find the most recent
  -- untraveled game options.
  procedure Find_Last_Untraveled_Option (Total_Game        : in String;
                                         Total_Game_Length : in Integer;
                                         Game_Branch        : out String;
                                         Game_Branch_Length : out Integer;
                                         Option        : out String;
                                         Option_Length : out Integer;
                                         Option_Identifier        : out String;
                                         Option_Identifier_Length : out Integer;
                                         Option_Searched_Point : out Integer);

end Create_Game;
