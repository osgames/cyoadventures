-------------------------------------------------------------------------------
--                                                                           --
--  Filename: create_adventure_game.adb                                      --
--                                                                           --
--  Date:     7/24/2012                                                      --
--                                                                           --
--  Purpose:  Implementation of "create a new game" procedures.              --
--            (See cyo_adventures_main.adb for full program description.     --
--                                                                           --
--  Legal licensing note:                                                    --
--                                                                           --
--    Copyright (c) 2012 Matt Davison                                        --
--                                                                           --
--  This program is free software; you can redistribute it and/or modify it  --
--  under the terms of the GNU General Public License as published by the    --
--  Free Software Foundation; either version 2 of the License, or (at your   --
--  option) any later version. This program is distributed in the hope that  --
--  it will be useful, but WITHOUT ANY WARRANTY; without even the implied    --
--  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See    --
--  the GNU General Public License for more details.  You should have        --
--  received a copy of the GNU General Public License along with this        --
--  program; if not, write to the Free Software Foundation, Inc., 59 Temple  --
--  Place - Suite 330, Boston, MA 02111-1307, USA.                           --
--                                                                           --
-------------------------------------------------------------------------------

with Ada.Text_IO;
with Ada.Strings.Unbounded;
with Ada.Strings.Fixed;
with Ada.Streams.Stream_IO;
with Ada.Directories;
with Shared_Game_Functions;

package body Create_Game is

  package StrFixed renames Ada.Strings.Fixed;
  package StreamIO renames Ada.Streams.Stream_IO;
  package Shared   renames shared_game_functions;

  -- Create new game.
  procedure Create_New_Game is

    Game_Name        : String(1 .. 500);
    Game_Name_Length : Integer;
    Total_Game       : StrUnbounded.Unbounded_String;

  begin
    Ada.Text_IO.Put_Line("******************************");
    Ada.Text_IO.New_Line;
    Ada.Text_IO.Put_Line("  Please choose a name for your game:");
    Ada.Text_IO.New_Line;

    -- Get game title.
    Ada.Text_IO.Get_Line(Game_Name, Game_Name_Length);

    -- Get game's introduction.
    Get_Game_Introduction (Total_Game);

    -- Get rest of the game.
    Get_Game_Branches (Total_Game);

    -- The new game has been successfully completed.
    -- Save to file, if user chooses.
    Save_Completed_Game (Game_Name, Game_Name_Length, Total_Game);

  end Create_New_Game;


  -- Prompt user for game's introduction, and save in Total_Game.
  procedure Get_Game_Introduction (Total_Game : out
                                   StrUnbounded.Unbounded_String) is

    User_Input        : String(1 .. 5000);
    User_Input_Length : Integer;

  begin
    Ada.Text_IO.New_Line;
    Ada.Text_IO.Put_Line("  Type the introduction to your adventure game:");
    Ada.Text_IO.New_Line;

    -- Get user's input.
    Ada.Text_IO.Get_Line(User_Input, User_Input_Length);

    -- Append introduction to the total game.
    StrUnbounded.Append(Total_Game, User_Input(1..User_Input_Length) & "%%%");

    -- Get first in-game options.
    Ada.Text_IO.New_Line;
    Ada.Text_IO.Put     ("  What can the user choose to do next?  ");
    Ada.Text_IO.Put_Line("Type options below.  Type ""x"" to end." );

    for Option_Count in Positive'Range loop
      Ada.Text_IO.Put("  Option " &
                      Shared.Strip_Leading_Blank(Integer'Image(Option_Count)) &
                      ": ");

      -- Get in-game option from the user.
      Ada.Text_IO.Get_Line(User_Input, User_Input_Length);
      if User_Input(1 .. 1) = "x" or User_Input_Length = 0 then
        exit;
      end if;

      -- Append option to the total game.
      StrUnbounded.Append (Total_Game, "*0-" &
                           Shared.Strip_Leading_Blank(Integer'Image(
                           Option_Count)) & "%" & User_Input(1 ..
                           User_Input_Length) & "%%");
    end loop;

  end Get_Game_Introduction;


  -- Get rest of the game after the introduction.  Save in Total_Game.
  procedure Get_Game_Branches (Total_Game : in out
                               StrUnbounded.Unbounded_String) is

    User_Input        : String(1 .. 5000);
    User_Input_Length : Integer;

    Game_Branch        : String(1 .. 5000);
    Game_Branch_Length : Integer;

    Option_Identifier        : String(1 .. 100);
    Option_Identifier_Length : Integer;

    Option        : String(1 .. 5000);
    Option_Length : Integer;

    Option_Searched_Point : Integer;

  begin
    loop
      -- Append "end options" marker to the total game so far.
      StrUnbounded.Append(Total_Game, "~");

      -- Get variables for the previous untraveled branch.
      Find_Last_Untraveled_Option (StrUnbounded.To_String(Total_Game),
                                   StrUnbounded.To_String(Total_Game)'Length,
                                   Game_Branch,
                                   Game_Branch_Length,
                                   Option,
                                   Option_Length,
                                   Option_Identifier,
                                   Option_Identifier_Length,
                                   Option_Searched_Point);

      exit when Game_Branch_Length = 0;

      -- Mark with "@" that the branch has now been traveled.
      StrUnbounded.Replace_Slice(Total_Game, Option_Searched_Point,
                                 Option_Searched_Point, "@");

      Ada.Text_IO.New_Line;
      Ada.Text_IO.Put_Line("------------------------------");
      Ada.Text_IO.New_Line;

      -- Print text of the game branch now being traveled.
      Ada.Text_IO.Put_Line("  Recap of game branch " &
                           Option_Identifier(1 .. Option_Identifier_Length) &
                           ":");

      Ada.Text_IO.New_Line;
      Ada.Text_IO.Put_Line(Game_Branch(1..Game_Branch_Length));
      Ada.Text_IO.New_Line;

      Ada.Text_IO.Put_Line("  What happens after """ & Option(1 ..
                           Option_Length) & """?");

      Ada.Text_IO.New_Line;

      -- Get user's input for the game branch.
      Ada.Text_IO.Get_Line(User_Input, User_Input_Length);

      -- Append game branch to the total game.
      StrUnbounded.Append(Total_Game,
                          Option_Identifier(1..Option_Identifier_Length) & "%" &
                          User_Input(1..User_Input_Length) & "%%%");

      -- Get in-game options.
      loop
        Ada.Text_IO.New_Line;
        Ada.Text_IO.Put_Line("  Continue this game branch? (y / n)");
        User_Input(1..1) := "z";
        Ada.Text_IO.Get_Line(User_Input, User_Input_Length);

        if User_Input(1..1) = "y" then
          -- Add options to this game path.
          Ada.Text_IO.Put     ("  What can the user choose next? (type ""x"" ");
          Ada.Text_IO.Put_Line("or Enter to end options)");
          for Option_Count in Positive'Range loop

            Ada.Text_IO.Put("  Option " &
                            Shared.Strip_Leading_Blank(Integer'Image(
                            Option_Count)) & ": ");

            -- Get Option.
            Ada.Text_IO.Get_Line(User_Input, User_Input_Length);
            if User_Input(1 .. 1) = "x" or User_Input_Length = 0 then
              exit;
            end if;

            -- Append option to the total game.
            StrUnbounded.Append(Total_Game, "*" & Option_Identifier(1 ..
                                Option_Identifier_Length) & "-" &
                                Shared.Strip_Leading_Blank(Integer'Image(
                                Option_Count)) & "%" &
                                User_Input(1..User_Input_Length) & "%%");
          end loop;
          exit;
        elsif User_Input(1..1) = "n" then
          exit;
        else
          Ada.Text_IO.Put_Line("  Sorry, invalid choice!");
        end if;
      end loop;
    end loop;

  end Get_Game_Branches;


  -- Save game in the "games" directory.
  procedure Save_Completed_Game (Game_Name : in out String;
                                 Game_Name_Length : in out Integer;
                                 Total_Game : in
                                 StrUnbounded.Unbounded_String) is

    User_Input        : String(1 .. 10);
    User_Input_Length : Integer;

    File_Count  : Positive;

    Create_File : StreamIO.File_Type;
    Write_File  : Ada.Text_IO.File_Type;

  begin
    Ada.Text_IO.Put_Line("  Congrats!  You've finished creating your game!");

    loop
      Ada.Text_IO.Put_Line("  Save game? (y / n)");
      User_Input(1..1) := "z";
      Ada.Text_IO.Get_Line(User_Input, User_Input_Length);
      if User_Input(1..1) = "y" then

        -- Save game to disk.
        Ada.Text_IO.Put_Line("  Saving to file...");
        -- Create games directory if it does not exist.
        if Ada.Directories.Exists("./games") = false then
          Ada.Directories.Create_Directory("./games");
        end if;

        -- Create unique name for the game file, using the game's name.
        if File_Exists("games/" & Game_Name(Game_Name'First ..
                       Game_Name_Length) & ".cyo") = false then

          -- Filename does not exist.  Save with given name.
          Game_Name(Game_Name'First .. Game_Name_Length + 12) := "games/" &
            Game_Name(Game_Name'First .. Game_Name_Length) & ".cyo";

          Game_Name_Length := Game_Name_Length + 12;

          -- Create file.
          StreamIO.Create (File => Create_File,
                           Mode => StreamIO.Out_File,
                           Name => Game_Name(Game_Name'First ..
                                             Game_Name_Length));

          StreamIO.Close (Create_File);

          -- Open the game file for writing.
          Ada.Text_IO.Open (File => Write_File,
                            Name => Game_Name(Game_Name'First ..
                                              Game_Name_Length),
                            Mode => Ada.Text_IO.Out_File);
        else

          -- Filename already exists.  Append a number to the given name.
          File_Count := 1;
          loop
            if File_Exists("games/" & Game_Name(Game_Name'First..
                           Game_Name_Length) & "(" &
                           Shared.Strip_Leading_Blank(
                           Integer'Image(File_Count)) & ").cyo") = false then

              -- Filename does not exist yet with this number appended to it.
              -- Change Game_Name to have File_Count appended to it.
              Game_Name(Game_Name'First .. Game_Name_Length + 14 +
                Shared.Strip_Leading_Blank(Integer'Image(File_Count))'Length) :=
                "games/" & Game_Name(Game_Name'First..Game_Name_Length) &
                "(" & Shared.Strip_Leading_Blank(Integer'Image(File_Count)) &
                ").cyo";

              Game_Name_Length := Game_Name_Length + 14 +
                Shared.Strip_Leading_Blank(Integer'Image(File_Count))'Length;

              -- Create file.
              StreamIO.Create (File => Create_File,
                               Mode => StreamIO.Out_File,
                               Name => Game_Name(Game_Name'First..
                                                 Game_Name_Length));
              StreamIO.Close (Create_File);

              -- Open the file for writing.
              Ada.Text_IO.Open (File => Write_File,
                                Name => Game_Name(Game_Name'First..
                                                  Game_Name_Length),
                                Mode => Ada.Text_IO.Out_File);
              exit;
            end if;
            File_Count := File_Count + 1;
          end loop;
        end if;

        -- Write to the file.
        Ada.Text_IO.Put  (Write_File, StrUnbounded.To_String(Total_Game));
        Ada.Text_IO.Close(Write_File);

        -- Give "saved" confirmation message.
        Ada.Text_IO.New_Line;
        Ada.Text_IO.Put_Line("  Saved file: """ &
                             Ada.Directories.Current_Directory & "\games\" &
                             Game_Name(Game_Name'First + 8 ..
                             Game_Name_Length) & """");

        Ada.Text_IO.New_Line;
        Ada.Text_IO.Put     ("  *Please upload your .cyo file to the open ");
        Ada.Text_IO.Put_Line("source directory, so we can expand ");
        Ada.Text_IO.Put_Line("  our selection of downloadable games!*");

        exit;
      elsif User_Input(1..1) = "n" then
        exit;
      else
        Ada.Text_IO.Put_Line("  Sorry, invalid choice!");
      end if;
    end loop;

    Ada.Text_IO.New_Line;
    Ada.Text_IO.Put_Line("  Thank you for using C.Y.O. Adventures!");
    Ada.Text_IO.New_Line;
    Ada.Text_IO.Put_Line("******************************");
    Ada.Text_IO.New_Line;

  end Save_Completed_Game;


  -- Return true if file Name exists.  Return false if it does not exist.
  function File_Exists (Name : String) return Boolean is

    The_File : Ada.Text_IO.File_Type;

  begin
    Ada.Text_IO.Open (The_File, Ada.Text_IO.In_File, Name);
    Ada.Text_IO.Close (The_File);
    return True;

  exception
    when Ada.Text_IO.Name_Error =>
      return False;

  end File_Exists;


  -- Search backwards from the end of the total game to find the most recent
  -- untraveled game options.  (This code is compatible with Ada '95.  It can
  -- be simplified using the Ada.Strings.Fixed.Index funtion from Ada 2005.)
  procedure Find_Last_Untraveled_Option (Total_Game        : in String;
                                         Total_Game_Length : in Integer;
                                         Game_Branch        : out String;
                                         Game_Branch_Length : out Integer;
                                         Option        : out String;
                                         Option_Length : out Integer;
                                         Option_Identifier        : out String;
                                         Option_Identifier_Length : out Integer;
                                         Option_Searched_Point : out Integer) is
    Game_Branch_Start : Integer;
    Game_Branch_End : Integer;

    Option_Identifier_Start : Integer := 0;
    Option_identifier_End : Integer;

    Option_Start : Integer;
    Option_End : Integer;

  begin

    -- Search backwards for the last position with a %%%.
    Find_Option_Start:
    for i in reverse 1 .. Total_Game_Length - 2 loop
      if Total_Game (i..i+2) = "%%%" then
        Game_Branch_End := i;

        -- Check if there are any untraveled options in the game branch.
        for j in i .. Total_Game_Length - 2 loop
          if Total_Game (j .. j + 2) = "%%*" then

            -- Found an untraveled option.
            Option_Identifier_Start := j + 3;

            -- Record location of the "*" (not searched yet) mark.
            Option_Searched_Point := j + 2;

            exit Find_Option_Start;

          elsif Total_Game (j .. j + 2) = "%%~" then
            -- No untraveled options in this game branch.
            exit;
          end if;
        end loop;
      end if;
    end loop Find_Option_Start;

    if Option_Identifier_Start = 0 then
      -- There are no more untraveled options.
      Game_Branch_Length := 0;
      Option_Identifier_Length := 0;
      Option_Length := 0;
      return;
    end if;

    -- Find Game_Branch_Start.
    for i in reverse 1 .. Game_Branch_End - 1 loop
      if Total_Game (i..i+2) = "%%~" or i = 1 then

        if i /= 1 then
          for j in i + 3 .. Game_Branch_End - 1 loop
            if Total_Game (j .. j) = "%" then
              Game_Branch_Start := j + 1;
            end if;
          end loop;
        else
          Game_Branch_Start := i;
        end if;

        exit;
      end if;
    end loop;

    Game_Branch_Length := Game_Branch_End - Game_Branch_Start;

    Game_Branch(Game_Branch'First .. Game_Branch_Length) :=
      Total_Game(Game_Branch_Start .. Game_Branch_End - 1);

    -- Find the option's identifier string.
    for i in Option_Identifier_Start .. Total_Game_Length loop
      if Total_Game (i .. i) = "%" then
        Option_identifier_End := i;
        exit;
      end if;
    end loop;

    -- Find this option's identifier information.
    Option_Identifier_Length := Option_Identifier_End - Option_Identifier_Start;

    Option_Identifier(Option_Identifier'First .. Option_Identifier_Length) :=
      Total_Game(Option_Identifier_Start .. Option_Identifier_End - 1);

    Option_Start := Option_Identifier_End + 1;

    -- Find the end of the option.
    for i in Option_Start .. Total_Game_Length - 1 loop
      if Total_Game (i .. i + 1) = "%%" then
        Option_End := i;
        exit;
      end if;
    end loop;

    -- Find the untraveled option for this game branch.
    Option_Length := Option_End - Option_Start;

    Option(Option'First .. Option_Length) :=
      Total_Game(Option_Start .. Option_End - 1);

  end Find_Last_Untraveled_Option;

end Create_Game;
