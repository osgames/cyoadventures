-------------------------------------------------------------------------------
--                                                                           --
--  Filename: shared_game_functions.ads                                      --
--                                                                           --
--  Date:     7/24/2012                                                      --
--                                                                           --
--  Purpose:  Contains the specification for shared functions.               --
--            (See cyo_adventures_main.adb for full program description.     --
--                                                                           --
--  Legal licensing note:                                                    --
--                                                                           --
--    Copyright (c) 2012 Matt Davison                                        --
--                                                                           --
--  This program is free software; you can redistribute it and/or modify it  --
--  under the terms of the GNU General Public License as published by the    --
--  Free Software Foundation; either version 2 of the License, or (at your   --
--  option) any later version. This program is distributed in the hope that  --
--  it will be useful, but WITHOUT ANY WARRANTY; without even the implied    --
--  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See    --
--  the GNU General Public License for more details.  You should have        --
--  received a copy of the GNU General Public License along with this        --
--  program; if not, write to the Free Software Foundation, Inc., 59 Temple  --
--  Place - Suite 330, Boston, MA 02111-1307, USA.                           --
--                                                                           --
-------------------------------------------------------------------------------

package Shared_Game_Functions is

  -- Strip number's leading blank, if there is one.
  function Strip_Leading_Blank (Str : String) return String;

end Shared_Game_Functions;
