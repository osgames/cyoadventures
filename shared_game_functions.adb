-------------------------------------------------------------------------------
--                                                                           --
--  Filename: shared_game_functions.adb                                      --
--                                                                           --
--  Date:     7/24/2012                                                      --
--                                                                           --
--  Purpose:  Contains implementation for shared functions.                  --
--            (See cyo_adventures_main.adb for full program description.     --
--                                                                           --
--  Legal licensing note:                                                    --
--                                                                           --
--    Copyright (c) 2012 Matt Davison                                        --
--                                                                           --
--  This program is free software; you can redistribute it and/or modify it  --
--  under the terms of the GNU General Public License as published by the    --
--  Free Software Foundation; either version 2 of the License, or (at your   --
--  option) any later version. This program is distributed in the hope that  --
--  it will be useful, but WITHOUT ANY WARRANTY; without even the implied    --
--  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See    --
--  the GNU General Public License for more details.  You should have        --
--  received a copy of the GNU General Public License along with this        --
--  program; if not, write to the Free Software Foundation, Inc., 59 Temple  --
--  Place - Suite 330, Boston, MA 02111-1307, USA.                           --
--                                                                           --
-------------------------------------------------------------------------------

package body Shared_Game_Functions is

  -- Strip number's leading blank, if there is one.
  function Strip_Leading_Blank (Str : String) return String is
  begin
    if Str (Str'First) = ' ' then
      return Str (1 + Str'First .. Str'Last);
    else
      return Str;
    end if;
  end Strip_Leading_Blank;

end Shared_Game_Functions;
