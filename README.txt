
C.Y.O. Adventures is an Ada 2005 compatible text-based game player 
and game generator.  The games are structured as "decision trees."  
You can choose options at each game branch to decide the path you 
want to go in the game.

There are some games included in the "games" directory.  Users can 
generate their own text-based games with the program.  Please 
upload any games you make to the open source collection, if you 
want to share your games with others. 

This is free software.  See the file COPYING for copying 
permission.  

Any comments on the program would be greatly appreciated.  Please 
send comments to mattedavison1@gmail.com.

Matt Davison