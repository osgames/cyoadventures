-------------------------------------------------------------------------------
--                                                                           --
--  Filename: play_adventure_game.adb                                        --
--                                                                           --
--  Date:     7/24/2012                                                      --
--                                                                           --
--  Purpose:  Implementation of "play a game" procedures.                    --
--            (See cyo_adventures_main.adb for full program description.     --
--                                                                           --
--  Legal licensing note:                                                    --
--                                                                           --
--    Copyright (c) 2012 Matt Davison                                        --
--                                                                           --
--  This program is free software; you can redistribute it and/or modify it  --
--  under the terms of the GNU General Public License as published by the    --
--  Free Software Foundation; either version 2 of the License, or (at your   --
--  option) any later version. This program is distributed in the hope that  --
--  it will be useful, but WITHOUT ANY WARRANTY; without even the implied    --
--  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See    --
--  the GNU General Public License for more details.  You should have        --
--  received a copy of the GNU General Public License along with this        --
--  program; if not, write to the Free Software Foundation, Inc., 59 Temple  --
--  Place - Suite 330, Boston, MA 02111-1307, USA.                           --
--                                                                           --
-------------------------------------------------------------------------------

with Ada.Text_IO;
with Ada.Strings.Fixed;
with Ada.Directories;
with Shared_Game_Functions;

package body Play_Game is

  package Dirs     renames Ada.Directories;
  package StrFixed renames Ada.Strings.Fixed;
  package Shared   renames shared_game_functions;

  -- Load list of ".cyo" files in the "games" directory.  Get the user's
  -- selection, and run the game.
  procedure Play_Saved_Game is

    Total_Game : StrUnbounded.Unbounded_String;

  begin
    Ada.Text_IO.New_Line;
    Ada.Text_IO.Put_Line("  Choose an adventure game from the list.");

    -- Load user's game selection into Total_Game.
    Get_Game_Selection (Total_Game);

    if StrUnbounded.To_String(Total_Game) /= "~" then
      -- Run the game.
      Run_Game (Total_Game);
    end if;

  end Play_Saved_Game;


  -- List games in the "games" directory, and load the user's selection.
  procedure Get_Game_Selection (Total_Game : out
                                StrUnbounded.Unbounded_String) is

    User_Decision        : String(1..10);
    User_Decision_Length : Integer;

    Search  : Dirs.Search_Type;

    Dir_Ent        : Dirs.Directory_Entry_Type;
    Dir_Ent_Length : Integer;

     -- Array for filenames.
    File_Names : array (1..200) of String(1..200);

    -- Array for filename lengths.
    File_Names_Lengths : array (1..200) of Integer;

    -- Iterator for game files in "games" directory.
    File_Name_Count : Integer := 0;

    File_Selection  : Integer;

    Total_Game_Temp_Length : Integer := 5000;
    Total_Game_Temp        : String(1..Total_Game_Temp_Length);

    Read_File : Ada.Text_IO.File_Type;

  begin
    -- Print column headers.
    Ada.Text_IO.Put ("   File Name:");
    Ada.Text_IO.Set_Col (40);
    Ada.Text_IO.Put ("  File Size:");
    Ada.Text_IO.New_Line;

    -- Create "games" directory if it does not exist.
    if Dirs.Exists("./games") = false then
      Dirs.Create_Directory("./games");
    end if;

    Dirs.Start_Search (Search, "./games", "");

    -- Print game names and file sizes.
    while Dirs.More_Entries (Search) loop
      Dirs.Get_Next_Entry (Search, Dir_Ent);
      Dir_Ent_Length := Dirs.Simple_Name (Dir_Ent)'Length;

      if Dirs.Simple_Name (Dir_Ent)'Length >= 4 then

        if Dirs.Simple_Name (Dir_Ent)(Dir_Ent_Length - 3 .. Dir_Ent_Length) =
          ".cyo" then

           -- Put filename in File_Names array.
          File_Name_Count := File_Name_Count + 1;

          File_Names(File_Name_Count)(1 .. Dir_Ent_Length) :=
            Dirs.Simple_Name (Dir_Ent)(1 .. Dir_Ent_Length);

           -- Put filename length in File_Names_Lengths array.
          File_Names_Lengths(File_Name_Count) := Dir_Ent_Length;

          -- Print file name to screen.
          Ada.Text_IO.Put ("  Press " &
                           Shared.Strip_Leading_Blank(Integer'Image(
                           File_Name_Count)) & ": " & Dirs.Simple_Name (
                           Dir_Ent)(1 .. Dir_Ent_Length - 4));

          Ada.Text_IO.Set_Col (40);

          -- Print file size to screen.
          Ada.Text_IO.Put (Dirs.File_Size'Image(Dirs.Size("./games/" &
                           Dirs.Simple_Name (Dir_Ent)(1 .. Dir_Ent_Length))) &
                           " bytes");

          Ada.Text_IO.New_Line;
        end if;
      end if;
    end loop;

    Dirs.End_Search (Search);

    -- Add "Main Menu" option to the choices list.
    File_Name_Count := File_Name_Count + 1;

    Ada.Text_IO.Put_Line ("  Press " &
                          Shared.Strip_Leading_Blank(Integer'Image(
                          File_Name_Count)) & ": Main Menu");

    -- Get user's selection on what game to play.
    loop
      User_Decision(1..1) := "x";
      Ada.Text_IO.Get_Line(User_Decision, User_Decision_Length);

      begin
      File_Selection := Integer'Value(User_Decision(1 .. User_Decision_Length));
      if File_Selection = File_Name_Count then
        -- User chose to return to the main menu.
        StrUnbounded.Set_Unbounded_String(Total_Game, "~");
        Ada.Text_IO.New_Line;
        return;
      end if;
       -- Verify that user selection is valid.
      if File_Selection >= 1 and File_Selection <= File_Name_Count then
        exit;
      else
          Ada.Text_IO.New_Line;
          Ada.Text_IO.Put_Line("  Sorry, invalid choice!");
      end if;
       exception
        when others =>
          Ada.Text_IO.New_Line;
          Ada.Text_IO.Put_Line("  Sorry, invalid choice!");
      end;
    end loop;
     -- Open file chosen by the user.
    Ada.Text_IO.Open (File => Read_File,
                      Name => "./games/" & File_Names(File_Selection)(1 ..
                              File_Names_Lengths(File_Selection)),
                      Mode => Ada.Text_IO.In_File);

    -- Load the file into the Total_Game variable.
    while not Ada.Text_IO.end_Of_File (Read_File) loop

      Total_Game_Temp(1 .. 5000) := StrFixed."*"(5000, '~');
      Ada.Text_IO.Get_Line(File => Read_File,
                           Item => Total_Game_Temp,
                           Last => Total_Game_Temp_Length);

      StrUnbounded.Append(Total_Game, Total_Game_Temp);

    end loop;
    Ada.Text_IO.Close(Read_File);
  end Get_Game_Selection;


  -- Run the game in the parameter Total_Game.
  procedure Run_Game(Total_Game : in StrUnbounded.Unbounded_String) is

    User_Decision        : String(1..10);
    User_Decision_Length : Integer;

    Option_Number : Integer;

    Game_Branch_Start : Integer := 1;
    Game_Branch_End   : Integer;

    Option_Count  : Integer;
    Option_Start  : Integer;
    Option_End    : Integer;
    Option_Length : Integer;

    Option_Array  : array(1..50) of String(1..500);

    Option_Text_Start : Integer;

    Branch_ID_Tag_End : Integer;

  begin
    Ada.Text_IO.New_Line;
    Ada.Text_IO.Put_Line("******************************");
    Ada.Text_IO.Put_Line("  Press ""0"" at any time to return to main menu.");

    loop
      Ada.Text_IO.New_Line;

      Game_Branch_End :=
        StrFixed.Index(Source  => StrUnbounded.To_String(Total_Game),
                       Pattern => "%%%",
                       From    => Game_Branch_Start) - 1;

       -- Print current game branch.
       Ada.Text_IO.Put_Line (StrUnbounded.To_String(Total_Game)(
                             Game_Branch_Start .. Game_Branch_End));


      exit when StrUnbounded.To_String(Total_Game)(Game_Branch_End + 4 ..
                                                   Game_Branch_End + 4) = "~";

      Ada.Text_IO.New_Line;
      Ada.Text_IO.Put_Line("  What do you want to do next?");
      Ada.Text_IO.New_Line;

      -- Print game options for this game branch.
      Option_Start := Game_Branch_End + 5;
      Option_Count := 1;
      loop
        Option_End :=
          StrFixed.Index(Source  => StrUnbounded.To_String(Total_Game),
                         Pattern => "%%",
                         From    => Option_Start) - 1;

        Option_Length := Option_End - Option_Start + 1;

        -- Put the game option into the Option_Array.
        Option_Array(Option_Count)(1.. Option_Length) :=
          StrUnbounded.To_String(Total_Game)(Option_Start .. Option_End);

        Option_Text_Start :=
          StrFixed.Index(Source  => Option_Array(Option_Count),
                         Pattern => "%") + 1;

        -- Print the game option to the screen.
        Ada.Text_IO.Put_Line("  Press " &
                             Shared.Strip_Leading_Blank(
                             Integer'Image(Option_Count)) & ": " &
                             Option_Array(Option_Count)(Option_Text_Start ..
                             Option_Length));

        exit when StrUnbounded.To_String(Total_Game) (Option_End + 3 ..
                                                      Option_End + 3) = "~";

        Option_Count := Option_Count + 1;
        Option_Start := Option_End + 4;
      end loop;

      -- Get user's option choice.
      loop
        User_Decision(1..1) := "x";
        Ada.Text_IO.Get_Line(User_Decision, User_Decision_Length);
        begin
          Option_Number :=
            Integer'Value(User_Decision(1 .. User_Decision_Length));

          -- Exit the adventure game if input is "0".
          if Option_Number = 0 then
            Ada.Text_IO.New_Line;
            return;
          end if;

          -- Verify that the option choice is valid.
          if Option_Number >= 1 and Option_Number <= Option_Count then
            exit;
          else
              Ada.Text_IO.New_Line;
              Ada.Text_IO.Put_Line("  Sorry, invalid choice!");
          end if;
          -- Exception if invalid choice.
          exception
            when others =>
              Ada.Text_IO.New_Line;
              Ada.Text_IO.Put_Line("  Sorry, invalid choice!");
        end;
      end loop;

      -- Get the choice's ID tag.
      Branch_ID_Tag_End :=
        StrFixed.Index(Source  => Option_Array(Option_Number),
                       Pattern => "%") - 1;

      -- Jump to the next game branch, based on the user's option choice.
      Game_Branch_Start :=
        StrFixed.Index(Source  => StrUnbounded.To_String(Total_Game),
                       Pattern => "%%~" & Option_Array(Option_Number)(1 ..
                                          Branch_ID_Tag_End),
                       From    => Option_End) + 4 + Branch_ID_Tag_End;

    end loop;

    -- The game has completed.
    Ada.Text_IO.Put_Line ("  The End.");
    Ada.Text_IO.New_Line;
    Ada.Text_IO.Put      ("******************************");

    -- Wait for any user input to exit back to the main menu.
    Ada.Text_IO.Get_Line(User_Decision, User_Decision_Length);
    Ada.Text_IO.New_Line;
  end Run_Game;

end Play_Game;
