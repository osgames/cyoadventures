-------------------------------------------------------------------------------
--                                                                           --
--  Filename: cyo_adventures_main.adb                                        --
--                                                                           --
--  Date:     7/24/2012                                                      --
--                                                                           --
--  Purpose:  This is the main program for C.Y.O. Adventures (Choose Your    --
--            Own Adventures).  It is an Ada 2005 compatible text-based      --
--            game player and game generator.  The games are structured as   --
--            "decision trees."  You can choose options at each game branch  --
--            to decide the path you want to go in the game.                 --                                       --
--                                                                           --
--  Comments: This file contains the main menu for the program.  There are   --
--            some games included in the "games" directory.  Users can       --
--            generate their own text-based games with the program.          --
--            Please upload any games you make to the open source            --
--            collection, if you want to share your games with others.       --                                              --
--                                                                           --
--  Legal licensing note:                                                    --
--                                                                           --
--    Copyright (c) 2012 Matt Davison                                        --
--                                                                           --
--  This program is free software; you can redistribute it and/or modify it  --
--  under the terms of the GNU General Public License as published by the    --
--  Free Software Foundation; either version 2 of the License, or (at your   --
--  option) any later version. This program is distributed in the hope that  --
--  it will be useful, but WITHOUT ANY WARRANTY; without even the implied    --
--  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See    --
--  the GNU General Public License for more details.  You should have        --
--  received a copy of the GNU General Public License along with this        --
--  program; if not, write to the Free Software Foundation, Inc., 59 Temple  --
--  Place - Suite 330, Boston, MA 02111-1307, USA.                           --
--                                                                           --
-------------------------------------------------------------------------------

with Ada.Text_IO;
with Play_Game;
with Create_Game;

procedure Cyo_Adventures_Main is

  User_Decision        : String(1..10);
  User_Decision_Length : Integer;

begin
  -- Print greeting.
  Ada.Text_IO.New_Line;
  Ada.Text_IO.Put     ("  Welcome to C.Y.O. Adventure Games: ");
  Ada.Text_IO.Put_Line("Choose and Build Your Own Text Adventures!");
  Ada.Text_IO.New_Line;

  loop
    -- Print main menu.
    Ada.Text_IO.Put_Line("  What would you like to do?");
    Ada.Text_IO.Put_Line("  1. Go on an adventure.");
    Ada.Text_IO.Put_Line("  2. Create a new adventure tale.");
    Ada.Text_IO.Put_Line("  3. Quit.");
    Ada.Text_IO.New_Line;

    User_Decision(1..1) := "0";
    Ada.Text_IO.Get_Line(User_Decision, User_Decision_Length);

    if User_Decision(1..1) = "1" then
      Play_Game.Play_Saved_Game;
    elsif User_Decision(1..1) = "2" then
      Create_Game.Create_New_Game;
    elsif User_Decision(1..1) = "3" then
      exit;
    else
      Ada.Text_IO.New_Line;
      Ada.Text_IO.Put_Line("  Sorry, invalid choice!");
    end if;
  end loop;

end Cyo_Adventures_Main;
